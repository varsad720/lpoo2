# Metodo principal o de ejecucion en Python

# Importando una clase en Python
from modelos.usuario import Usuario
from modelos.producto import Producto

if __name__ == '__main__':
    print("Metodo Principal")

    #Crear instancia de USUARIO
    juan = Usuario()
    # print("EL rol es: ", juan.rol)
   # # Cambiando el valor de un atributo
    # juan.rol = "Admin"
    # print("Cambiando el rol: ", juan.rol)

    # Utilizando metodo registrar
    juan.registrar()

    print("Probando login")
    nom = input("Usuario: ")
    cred = input("Contraseña: ")

    # LLamado del metodo login
    juan.login(nom,cred)

    juan.reestablcer_pass()

    juan.consultar()

    juan.modificar()

    juan.consultar()

    juan.eliminar()

    print("--------Producto--------")

    soda = Producto()
    soda.agregar()
    soda.ver()
    soda.modificar()
    soda.ver()
    soda.vender()
    soda.eliminar()

