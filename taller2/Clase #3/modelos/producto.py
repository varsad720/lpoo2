from uuid import uuid3
class Producto:
    id=0
    nombre=""
    num_serie=""
    precio=0.0
    fecha_vencimiento=""
    detalle=""
    estado=False
    categoria=""
    cantidad=0

    def agregar(self):
        print("------Agregar Producto------")
        self.id = uuid3
        self.num_serie = input("Número de serie: ")
        self.nombre = input("Nombre del producto: ")
        self.detalle = input("Descripción: ")
        self.categoria = input("Categoría: ")
        self.precio = input("Precio del producto: ")
        self.cantidad = input("Cantidad a agregar: ")
        self.fecha_vencimiento = input("Fecha de expiración: ")
        self.estado = True
        print("Producto agregado con éxito.\n")

    def ver(self):
        print("------Ver Producto------")
        print("Número de serie:",self.num_serie,"\n","Nombre:",self.nombre,"\n",
              "Descrpción:",self.detalle,"\n","Categoría:",self.categoria,"\n",
              "Precio:",self.precio,"\n","Cantidad:",self.cantidad,"\n",
              "Fecha de vencimiento:",self.fecha_vencimiento,"\n","Disponibilidad:",self.estado,"\n",)

    def modificar(self):
        print("------Modificar Producto------")
        self.num_serie = input("Número de serie: ")
        self.nombre = input("Nombre del producto: ")
        self.detalle = input("Descripción: ")
        self.categoria = input("Categoría: ")
        self.precio = input("Precio del producto: ")
        self.cantidad = input("Cantidad a modificar: ")
        self.fecha_vencimiento = input("Fecha de expiración: ")
        print("Producto modificado con éxito.\n")

    def vender(self):
        print("------Vender Producto------")

        ventacant=int(input("Cantidad a vender: "))
        self.cantidad =  int(self.cantidad) - ventacant

        print("Producto vendido.\n"
              "Cantidad disponible:", self.cantidad)

        if self.cantidad==0:
            self.estado=False



    def eliminar(self):
        self.id = 0
        self.num_serie = ""
        self.nombre = ""
        self.detalle = ""
        self.categoria = ""
        self.precio = 0.0
        self.cantidad = 0
        self.fecha_vencimiento = ""
        self.estado = False

        print("\nProducto eliminado satisfactoriamente.")








