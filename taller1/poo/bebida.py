class Bebida:
    marca=""
    variante=""
    envase=""
    precio=""

    def indatos(self):
        print("Introduzca los datos de la bebida")
        marca=input("Marca de la bebida: ")
        variante=input("Variante: ")
        envase=input("Tipo de envase: ")
        precio=input("Precio: ")
        self.marca=marca
        self.variante=variante
        self.envase=envase
        self.precio=precio

    def showdata(self):
        print("Datos de la bebida:")
        print("La bebida es una",self.marca,self.variante,"con envase de",self.envase,"y un precio de",self.precio)

bebida1 = Bebida()
bebida1.indatos()
bebida1.showdata()

