class Mascota:
    tipo=""
    comida=""
    nombre=""

    def conocermascota(self):
        print("Datos mascota")
        tipo = input("Tipo de animal: ")
        comida = input("Comida: ")
        nombre = input("Nombre de la mascota: ")
        self.tipo = tipo
        self.nombre = nombre
        self.comida = comida




    def comer(self):
        print("el ",self.tipo,self.nombre,"acaba de comer", self.comida)

gato1 = Mascota()
gato1.conocermascota()
gato1.comer()

class Carro:
    marca=""
    año=""
    km=""
    color=""

    def pedirdatos(self):
        print("Ingrese los datos del vehículo")
        marca = input("Marca: ")
        año = input("Modelo: ")
        km = input("Kilometraje: ")
        color = input("Color: ")
        self.marca = marca
        self.color = color
        self.año = año
        self.km = km




    def mostrarkm(self):

        print("el vehículo",self.marca,"color",self.color,"modelo", self.año,"tiene",self.km,"km")

carro1 = Carro()
carro1.pedirdatos()
carro1.mostrarkm()

class Zapatilla:
    marca=""
    talla=""

    def indatos(self):
        print("Ingrese los datos de la zapatilla")
        marca=input("Marca: ")
        talla=input("Talla: ")
        self.marca=marca
        self.talla=talla

    def mostrartalla(self):
        print("La talla de las zapatillas marca",self.marca,"es:",self.talla)

zapatilla1= Zapatilla()
zapatilla1.indatos()
zapatilla1.mostrartalla()

class Celular:
    marca=""
    capacidad=""
    camaras=""
    tipopantalla=""

    def indatos(self):
        print("Ingrese las especificaciones del dispositivo:")
        marca = input("Marca: ")
        capacidad = input("Almacenamiento: ")
        camaras = input("Número de cámaras: ")
        tipopantalla = input("Tipo de pantalla: ")
        self.marca = marca
        self.capacidad = capacidad
        self.camaras = camaras
        self.tipopantalla = tipopantalla

    def showspecs(self):
        print("Especificaciones del dispositivo: ")
        print("Celular marca",self.marca,"con",self.capacidad,"de almacenamiento, cuenta con",self.camaras,"camara(s) y pantalla con tecnología",self.tipopantalla)

cel1 = Celular()

cel1.indatos()
cel1.showspecs()

class Moto:
    marca=""
    año=""
    km=""
    color=""

    def pedirdatos(self):
        print("Ingrese los datos de la motocicleta")
        marca = input("Marca: ")
        año = input("año: ")
        km = input("Kilometraje: ")
        color = input("Color: ")
        self.marca = marca
        self.color = color
        self.año = año
        self.km = km




    def mostrarmodelo(self):

        print("la moto",self.marca,"color",self.color,"de km",self.km ,"es del año",self.año)

moto1 = Moto()
moto1.pedirdatos()
moto1.mostrarmodelo()



