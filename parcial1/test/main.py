from paciente import Paciente
from clinica import Clinica
from menus import  Menus
from medico import Medico
if __name__ =='__main__':

    menus=Menus()
    medico=Medico()
    clinica=Clinica()
    paciente=Paciente()

    print("------>MENÚ PRINCIPAL<------")
    opciones = 0

    while opciones != 4:
        print(
            "\n- Presione 1 para MÉDICOS\n- Presione 2 para CLÍNICAS\n- Presione 3 para PACIENTES\n- Presione 4 para SALIR\n")
        opciones = int(input("Introduzca un número: "))



        if opciones == 1:

            menus.menumedico()
            medico.main()
        if opciones == 2:
            menus.menuclinica()
            clinica.main()


        if opciones == 3:
            menus.menupaciente()
            paciente.main()






