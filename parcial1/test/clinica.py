from menus import Menus
class Clinica:

    nombre=""
    ubicacion=""
    antiguedad=""
    capacidad=""
    estado=False

    def main(self):
        while True:
            menus = Menus()
            menus.opcionc = menus.menuclinica()

            if menus.opcionc == 1:
                print("------REGISTRO DE CLÍNICA-----\n")

                self.nombre = input("Nombre: ")
                self.ubicacion = input("Ubicación: ")
                self.antiguedad = input("Antigüedad: ")
                self.capacidad = input("Capacidad de pacientes: ")
                self.estado = True
                input("Clínica registrada correctamente")

            if menus.opcionc == 2:
                if self.estado == True:
                    print("------MODIFICAR DATOS DE LA CLÍNICA-----\n")
                    self.nombre = input("Nombre: ")
                    self.ubicacion = input("Ubicación: ")
                    self.antiguedad = input("Antigüedad: ")
                    self.capacidad = input("Capacidad de pacientes: ")
                else:
                    print("No se ha registrado una clínica")


            if menus.opcionc == 3:
                print(self.estado)
                if self.estado == True:
                    print("----- Datos de la clínica", self.nombre, "-----\n")
                    print("Nombre:", self.nombre, "\nUbicación:", self.ubicacion, "\nAntigüedad:", self.antiguedad, "\nCapacidad:", self.capacidad, "\n")
                else:
                    print("No se ha registrado una clínica")


            if menus.opcionc == 4:

                self.nombre  =""
                self.ubicacion =""
                self.antiguedad =""
                self.capacidad =""
                self.estado = False
                print("REGISTRO DE CLÍNICA ELIMINADO SATISFACTORIAMENTE.")
                input()

            elif menus.opcionc == 5:
                print("SALIENDO")
                break
        return