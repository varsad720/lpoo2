from menus import Menus
class Paciente:
    nombre=""
    apellido=""
    fecha_n=""
    cedula=""
    estado=False

    def main(self):
        while True:
            menus = Menus()
            menus.opcionp = menus.menupaciente()

            if menus.opcionp == 1:
                print("------REGISTRO DE PACIENTE-----\n")

                self.nombre = input("Nombre: ")
                self.apellido = input("Apellido: ")
                self.fecha_n = input("Fecha de nacimiento: ")
                self.cedula = input("Cédula o pasaporte: ")
                self.estado = True
                input("Paciente registrado correctamente")

            if menus.opcionp == 2:
                if self.estado == True:
                    print("------MODIFICAR DATOS DEL PACIENTE-----\n")
                    self.nombre = input("Nombre: ")
                    self.apellido = input("Apellido: ")
                    self.fecha_n = input("Fecha de nacimiento: ")
                    self.cedula = input("Cécula o pasaporte: ")
                else:
                    print("No se ha registrado un paciente")

            if menus.opcionp == 3:
                print(self.estado)
                if self.estado == True:
                    print("----- Datos del paciente", self.nombre, "-----\n")
                    print("Nombre:", self.nombre, "\nApellido:", self.apellido, "\nFecha de nacimiento:", self.fecha_n, "\nCédula:", self.cedula, "\n")
                else:
                    print("No se ha registrado un paciente")

            if menus.opcionp == 4:

                self.nombre  =""
                self.apellido =""
                self.fecha_n =""
                self.cedula =""
                self.estado = False
                print("REGISTRO DE PACIENTE ELIMINADO SATISFACTORIAMENTE.")
                input()

            elif menus.opcionp == 5:
                print("SALIENDO")
                break
        return




